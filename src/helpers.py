import os
import re
from collections import OrderedDict
from subprocess import check_call, CalledProcessError

from django.conf import settings
from pycel import ExcelCompiler

DEBUG = True


def print_if_debug(*args):
    if DEBUG:
        print(*args)


def to_decimal_degrees(value):
    print_if_debug(f"WGS84 coord (dms) {value[0]} {value[1]} {value[2]} {value[3]}")
    miliseconds = value[3]
    secs = value[2] + miliseconds / 1000000
    minutes = value[1] + secs / 60
    degrees = value[0] + minutes / 60
    return degrees


def convert_to_ITM(x, y):
    path = os.path.dirname(__file__)
    fname = os.path.join(path, "CoordinateConvert.xlsx")
    excel = ExcelCompiler(filename=fname)
    excel.evaluate('Main!C7')
    excel.evaluate('Main!D7')
    excel.set_value('Main!C4', x)
    excel.set_value('Main!D4', y)
    return excel.evaluate('Main!C7'), excel.evaluate('Main!D7')


def get_ITM_coords(x, y):
    x = to_decimal_degrees(x)
    y = to_decimal_degrees(y)
    print_if_debug(f"WGS84 {x},{y}")
    return convert_to_ITM(x, y)


def ifc_site(ifc_file):
    site_product = ifc_file.by_type("IfcSite")
    x, y = get_ITM_coords(site_product[0].RefLatitude, site_product[0].RefLongitude)
    print_if_debug(f"New ITM coords {x}, {y}")
    return x, y


def ifc_building_storey(ifc_file):
    build_storey_products = ifc_file.by_type("IfcBuildingStorey")
    build_storey_heights = []
    for i in range(len(build_storey_products) - 1):
        build_storey_heights.append(
            build_storey_products[i + 1].Elevation - build_storey_products[i].Elevation)
    build_storey_heights.append(0)
    rows = []
    for product, height in zip(build_storey_products, build_storey_heights):
        rows.append([product.Name, round(product.Elevation / 100, 2), round(height, 2)])

    return table_view("IfcBuildingStorey", "Model Floors", ["Name", "Level (m)", "Height (cm)"], rows)


def check_priority(floor, max_floor):
    return replace_list(floor,
                        ("Basement", "-1."),
                        ("Ground", "0."),
                        ("Floor", "1."),
                        ("Gallery", "2."),
                        ("Roof", "3."),
                        ("Others", "4")
                        )


def get_floor_name(search, long_name, floor):
    if re.search(rf"{search} -(\d*\.\d+|\d*)", long_name, re.IGNORECASE):
        return re.search(rf"{search} -(\d*\.\d+|\d*)", long_name, re.IGNORECASE).group().replace(f"{search} -", floor)
    elif re.search(rf"{search} (\d*\.\d+|\d*)", long_name, re.IGNORECASE):
        return re.search(rf"{search} (\d*\.\d+|\d*)", long_name).group().replace(f"{search} ", floor)
    else:
        return re.search(rf"{search}(\d*\.\d+|\d*)", long_name, re.IGNORECASE).group().replace(search, floor)


def get_floor_name_with_criteria(long_name, *args):
    for arg in args:
        if arg[0].lower() in long_name:
            return get_floor_name(arg[0], long_name, arg[1])
    return long_name


def replace_list(string, *args):
    for arg in args:
        string = string.replace(arg[0], arg[1])
    return string


def ifc_quantity_area(ifc_file):
    quantity_area_products = ifc_file.by_type("IfcQuantityArea")
    space_products = ifc_file.by_type("IfcSpace")
    rows = []
    i, j = 0, 0

    while i < len(space_products) and j < len(quantity_area_products):
        if quantity_area_products[j].Description == "area measured in geometry" and \
                quantity_area_products[j].Name == "GrossFloorArea":
            rows.append(
                dict(
                    room_name=space_products[i].LongName,
                    area_value=round(quantity_area_products[j].AreaValue, 2),
                    level=get_location(space=space_products[i]),
                    storey_name=get_storey_name(space=space_products[i])
                )
            )
            i += 1
        j += 1

    return rows


def format_rows(rows):
    list_view = list()
    total_area = 0

    storey_names = set(map(lambda x: (x.get('storey_name'), x.get('level')), rows))
    storeys = [(y, [r for r in rows if r.get('storey_name') == x]) for x, y in storey_names]

    for level, storey in sorted(storeys):
        storey_area = 0
        level = 0

        for room in storey:
            level = room.get('level', 0)
            storey_area += room.get('area_value', 0)
            total_area += room.get('area_value', 0)
            list_view.append([len(list_view) + 1, room.get('level'), room.get('room_name'), room.get('area_value', 0)])

        list_view.append(
            [
                len(list_view) + 1,
                f"<strong>{level}</strong>",
                "<strong>Total</strong>",
                f"<strong>{round(storey_area, 2)}</strong>"
            ]
        )
    list_view.append(
        [len(list_view) + 1, "", "<strong>Grand Total</strong>", f"<strong>{round(total_area, 2)}</strong>"]
    )

    return table_view(
        class_type="IfcQuantityArea",
        heading="Model Areas",
        headers=["#", "Level", "Room", "AreaValue"],
        rows=list_view
    )


def get_storey_name(space):
    for obj in space.Decomposes:
        if obj.RelatingObject.is_a('IfcBuildingStorey'):
            return obj.RelatingObject.LongName


def get_location(space):
    x, y, z = 0, 0, 0
    for obj in space.Decomposes:
        if obj.RelatingObject.is_a('IfcBuildingStorey'):
            location = obj.RelatingObject.ObjectPlacement.RelativePlacement.Location
            x, y, z = location.Coordinates
    return f'{round(int(z) / 100, 2)}'


def table_view(class_type, heading, headers, rows):
    return {
        "class_type": class_type,
        "heading": heading,
        "headers": headers,
        "rows": rows
    }


def convert_to_obj_mtl(file_path):
    try:
        check_call([settings.IFC_CONVERT_PATH, file_path, '-y'])
        return (
            f"{settings.MEDIA_URL}sample.obj".lstrip('/'),
            f"{settings.MEDIA_URL}sample.mtl".lstrip('/')
        )
    except CalledProcessError:
        return None, None
