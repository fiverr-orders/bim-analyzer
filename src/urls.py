"""
Definition of urls for DjangoWebMedEquip.
"""

from datetime import datetime
from django.urls import path, re_path
from django.contrib import admin
from django.contrib.auth.views import LoginView, LogoutView
from .views import main, logout_user, login_user, get_logs
from . import settings
from django.conf.urls.static import static
urlpatterns = [
    path('', main),
    path('login/', login_user),
    path('logout/', logout_user),
    path('ajax/get-logs/', get_logs),
    path('admin/', admin.site.urls),  # username=root, pass=root1133

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)