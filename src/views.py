from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.shortcuts import render
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
import ifcopenshell
from src.helpers import ifc_building_storey, ifc_quantity_area, ifc_site, convert_to_obj_mtl, format_rows


class LOGS:
    NOTHING = "No logs available"
    ERROR = "Error occurred: "
    DOWNLOAD_BEGIN = "Downloading for url (%s) began"
    WAITING = "Waiting: "
    URL_SET = "url (%s) set. Video name: %s."
    FINISHED = "Finished. "


logs = "empty"


class ResponseStrings:
    SUCCESS = "Success. "
    FAILED = "Failed. "
    PROCESSING_REQUEST = "Processing request"


def verify_session_var(request, var):
    if var not in request.session:
        return False
    return True


def main(request):
    if not request.user.is_authenticated:
        return render(request, "login.html", {})
    else:
        context = {"LOGS": LOGS}
        request.session["file_data"] = None
        if request.POST:
            file = request.FILES['ifc']
            request.session["file_data"] = {"name": file.name}

            ifc_path = handle_uploaded_file(file)
            ifc_file = ifcopenshell.open(ifc_path)
            rows = ifc_quantity_area(ifc_file)
            location = (ifc_site(ifc_file))
            ifc_path_obj, ifc_path_mtl = convert_to_obj_mtl(ifc_path)

            graph_rows = [[row.get('area_value'), row.get('room_name')] for row in rows]
            context["graph"] = {"legend": "Area Graph", "rows": graph_rows}
            context["table_model_floors"] = ifc_building_storey(ifc_file)
            context["table_model_areas"] = format_rows(rows)
            context["location"] = location
            context["obj"] = ifc_path_obj
            context['mtl'] = ifc_path_mtl

            request.session["location"] = location

        return render(request, "index.html", context)


def logout_user(request):
    if request.user.is_authenticated:
        request.session["file_data"] = None
        logout(request)
        # messages.success(request, "Successfully logout")
        return HttpResponseRedirect('/')
    raise Http404


def login_user(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        print(username, password, user)
        if user is not None:
            login(request, user)
            # messages.success(request, 'Login Successfully.')
        else:
            messages.error(request, 'Wrong username or password.')

    return HttpResponseRedirect('/')


def handle_uploaded_file(f):
    import os
    from . import settings
    abs_path = os.path.join(settings.MEDIA_ROOT, "sample.ifc")
    with open(abs_path, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    return abs_path


def get_logs(request):
    return HttpResponse(logs)
